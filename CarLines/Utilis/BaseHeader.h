
//
//  BaseHeader.h
//  CarLines
//
//  Created by 国信 on 16/3/7.
//  Copyright © 2016年 LYY. All rights reserved.
//

#ifndef BaseHeader_h
#define BaseHeader_h

//navigation bar color
#define NavBarColor [UIColor colorWithRed:(255 / 255.) green:(92 / 255.) blue:(0 / 255.) alpha:1]
//tabbar color
#define TabBarColor [UIColor colorWithRed:(255 / 255.) green:(92 / 255.) blue:(0 / 255.) alpha:1]

//宽度和高度自适应
#define SCREEN_WIDTH      [UIScreen mainScreen].bounds.size.width
#define SCREEN_HEIGHT     [UIScreen mainScreen].bounds.size.height

#define WIDTH(x) (x)*(SCREEN_WIDTH/414)
#define HEIGHT(y) (y)*(SCREEN_HEIGHT/736)
//自适应
#import "UIView+SDAutoLayout.h"
//网络请求
#import "AFNetworking.h"
//单例
#import "Singleton.h"
#endif /* BaseHeader_h */
