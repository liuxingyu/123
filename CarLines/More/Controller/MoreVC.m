//
//  MoreVC.m
//  CarLines
//
//  Created by 国信 on 16/3/7.
//  Copyright © 2016年 LYY. All rights reserved.
//

#import "MoreVC.h"

@implementation MoreVC

-(void)viewDidLoad{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor yellowColor]];
    [self setUpTableView];
}

-(void)setUpTableView{
    self.tableViewVC = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];
    [self.tableViewVC setBackgroundColor:[UIColor colorWithRed:(239 / 255.) green:(239 / 255.) blue:(239 / 255.) alpha:1]];
    self.tableViewVC.delegate = self;
    self.tableViewVC.dataSource = self;
    [self.view addSubview:self.tableViewVC];
    self.tableViewVC.scrollEnabled = NO;
    self.tableViewVC.showsVerticalScrollIndicator = NO;
}
//tableview 数据
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return HEIGHT(70);
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 1;
    }else if (section == 1){
        return 2;
    }else{
        return 5;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return HEIGHT(17);
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 2) {
        return HEIGHT(17);
    }else{
        return 0;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellReuseed = @"reused";
    MoreTCell *moreCell = [tableView dequeueReusableCellWithIdentifier:cellReuseed];
    if (cellReuseed) {
        moreCell = [[MoreTCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellReuseed];
        moreCell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (indexPath.section == 0) {
            [moreCell.iconImage setImage:[UIImage imageNamed:@"iconfont-saoyisaoqupiaoma"]];
            [moreCell.titleLabel setText:@"扫一扫"];
        }else if (indexPath.section == 1){
            if (indexPath.row == 0) {
                [moreCell.iconImage setImage:[UIImage imageNamed:@"iconfont-iconfonttupian"]];
                [moreCell.titleLabel setText:@"在移动网络下加载图片"];
            }else{
                [moreCell.iconImage setImage:[UIImage imageNamed:@"iconfont-lajixiang"]];
                [moreCell.titleLabel setText:@"清除缓存"];
            }
        }else{
            if (indexPath.row == 0) {
                [moreCell.iconImage setImage:[UIImage imageNamed:@"iconfont-shuaxin"]];
                [moreCell.titleLabel setText:@"检查更新 4.7.13"];

            }else if (indexPath.row == 1){
                [moreCell.iconImage setImage:[UIImage imageNamed:@"iconfont-daiyue"]];
                [moreCell.titleLabel setText:@"公告列表"];
            }else if (indexPath.row == 2){
                [moreCell.iconImage setImage:[UIImage imageNamed:@"iconfont-dianhua"]];
                [moreCell.titleLabel setText:@"客服电话  400-057-5210"];
            }else if (indexPath.row == 3){
                [moreCell.iconImage setImage:[UIImage imageNamed:@"iconfont-youxiang"]];
                [moreCell.titleLabel setText:@"客服邮箱  apao2o@163.com"];
            }else{
                [moreCell.iconImage setImage:[UIImage imageNamed:@"iconfont-guanyu"]];
                [moreCell.titleLabel setText:@"关于我们"];
            }
        }
    }
    return moreCell;

}

//tableview 代理

@end
