//
//  HomepageVC.m
//  CarLines
//
//  Created by 国信 on 16/3/7.
//  Copyright © 2016年 LYY. All rights reserved.
//

#import "HomepageVC.h"
#import "BaseHeader.h"
#import "AFNetworking.h"
#import "CityVC.h"
#import "CityModel.h"
@interface HomepageVC ()
@property (nonatomic, retain) NSMutableArray *citylistArray;
@end

@implementation HomepageVC

-(void)viewDidLoad{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor greenColor]];
    [self setUpTabBatItem];
    //获取城市数据
    [self getCitySourceData];

}
//设置左右tabbaritem
-(void)setUpTabBatItem{
    UIButton *searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [searchBtn setImage:[UIImage imageNamed:@"iconfont-wodedingdan11"] forState:UIControlStateNormal];
    CGSize size = searchBtn.currentImage.size;
    searchBtn.frame = CGRectMake(0, 0, size.width, size.height);
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:searchBtn];
    
    UIButton *cityBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [cityBtn setImage:[UIImage imageNamed:@"iconfont-iconfontxiala-2"] forState:UIControlStateNormal];
    CGSize csize = cityBtn.currentImage.size;
    cityBtn.frame = CGRectMake(0, 0, csize.width, csize.height);
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:cityBtn];
    [cityBtn addTarget:self action:@selector(selectorCityAction) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - 城市查询页面
-(void)selectorCityAction{
    CityVC *cityVC = [[CityVC alloc] init];
    [self.navigationController pushViewController:cityVC animated:YES];
}
#pragma mark - 数据解析
-(void)getCitySourceData{
    NSString *url = @"http://www.apao2o.com/mapi/index.php?requestData=eyJhY3QiOiJpbmRleCIsImNpdHlfaWQiOjIsImN0bCI6ImluaXQiLCJkZXZpY2VfdHlwZSI6ImFu%0AZHJvaWQiLCJmcm9tIjoiYW5kcm9pZCIsInNlc3NfaWQiOiJ1aWFkNXNhaHNlaHR0ZmVwaXU2MWF0%0AMzA2NyIsInZlcnNpb25fbmFtZSI6IjQuNy4xMyJ9%0A&i_type=0&r_type=1&ctl=init&act=index";
    
    AFHTTPSessionManager *sessionManager = [AFHTTPSessionManager manager];
    sessionManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/plain",@"text/json",@"application/json",@"text/javascript",@"text/html",nil];
    [sessionManager POST:url parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
        //下载进度
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        //回调成功
        self.citylistArray = [[NSMutableArray alloc] init];
        for (NSDictionary *dic in [responseObject objectForKey:@"citylist"]) {
            CityModel *model = [[CityModel alloc] initWithDictionary:dic];
            [Singleton shareInstance].title = [responseObject objectForKey:@"program_title"];
            [Singleton shareInstance].phone = [responseObject objectForKey:@"kf_phone"];
            [Singleton shareInstance].email = [responseObject objectForKey:@"kf_email"];
            [self.citylistArray addObject:model];
        }
        [Singleton shareInstance].cityArray = self.citylistArray;
        self.navigationItem.title = [Singleton shareInstance].title;
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        //回调失败
        
    }];
}
@end
