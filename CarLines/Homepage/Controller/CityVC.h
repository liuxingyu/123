//
//  CityVC.h
//  CarLines
//
//  Created by 国信 on 16/3/9.
//  Copyright © 2016年 LYY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseHeader.h"
@interface CityVC : UITableViewController<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, retain) UITableView *cityTableView;

@end
