//
//  TabBarController.m
//  CarLines
//
//  Created by 国信 on 16/3/8.
//  Copyright © 2016年 LYY. All rights reserved.
//

#import "TabBarController.h"
#import "NavigationBar.h"
#import "BaseHeader.h"
#import "HomepageVC.h"
#import "SellerVC.h"
#import "BuyVC.h"
#import "MineVC.h"
#import "MoreVC.h"

@interface TabBarController ()
@property (nonatomic, retain) HomepageVC *homepageVC;
@property (nonatomic, retain) SellerVC *sellerVC;
@property (nonatomic, retain) BuyVC *buyVC;
@property (nonatomic, retain) MineVC *mineVC;
@property (nonatomic, retain) MoreVC *moreVC;
@end

@implementation TabBarController
-(void)viewDidLoad{
    [super viewDidLoad];
    
    //首页
    _homepageVC = [[HomepageVC alloc] init];
    [self addChildViewController:_homepageVC title:@"首页" image:@"iconfont-icon-gohome"];
    //商家
    _sellerVC = [[SellerVC alloc] init];
    [self addChildViewController:_sellerVC title:@"商家" image:@"iconfont-shangjiachaxun"];
    //购物车
    _buyVC = [[BuyVC alloc] init];
    [self addChildViewController:_buyVC title:@"购物车" image:@"iconfont-fenxiang"];
    //我的
    _mineVC = [[MineVC alloc] init];
    [self addChildViewController:_mineVC title:@"我的" image:@"iconfont-wode"];
    //更多
    _moreVC = [[MoreVC alloc] init];
    [self addChildViewController:_moreVC title:@"更多" image:@"iconfont-gengduo"];
}

-(void)addChildViewController:(UIViewController *)childController title:(NSString *)title image:(NSString *)image{
    NavigationBar *homeNav = [[NavigationBar alloc] initWithRootViewController:childController];
    [homeNav.navigationBar setBarTintColor:NavBarColor];
    childController.title = title;//同时设置navgationcontroller 和 tabbar的title
    [homeNav.tabBarItem setImage:[UIImage imageNamed:image]];
    if (childController == _homepageVC) {
        childController.navigationItem.title = @"北方车联";
    }
    //设置导航栏 －－－标题颜色和字体
    NSMutableDictionary *textAttr = [NSMutableDictionary dictionary];
    textAttr [NSForegroundColorAttributeName] = [UIColor whiteColor];
    textAttr [NSFontAttributeName] = [UIFont systemFontOfSize:22.];
    [childController.navigationController.navigationBar setTitleTextAttributes:textAttr];
    
    [self addChildViewController:homeNav];
}

@end
