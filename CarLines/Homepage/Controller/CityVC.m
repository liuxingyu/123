//
//  CityVC.m
//  CarLines
//
//  Created by 国信 on 16/3/9.
//  Copyright © 2016年 LYY. All rights reserved.
//

#import "CityVC.h"
#import "CityModel.h"
@interface CityVC ()
//索引数组
@property (nonatomic, retain) NSMutableArray *dataSource;
//存在的城市字典
@property (nonatomic, retain) NSMutableDictionary *mutableDic;
//城市拼音首字母
@property (nonatomic, retain) NSString *str;
//索引提示label
@property (nonatomic, retain) UILabel *label;

@end

@implementation CityVC

-(void)viewDidLoad{
    [super viewDidLoad];
    [self setUpTableView];
    [self seperatorWord];
    //改变索引的颜色
    if ([self.tableView respondsToSelector:@selector(setSectionIndexColor:)]) {
        self.tableView.sectionIndexBackgroundColor = [UIColor clearColor];
        self.tableView.sectionIndexTrackingBackgroundColor = [UIColor clearColor];
    }
    self.label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, WIDTH(50), HEIGHT(50))];
    [self.label setBackgroundColor:[UIColor orangeColor]];
    [self.view addSubview:self.label];
    self.label.center = self.view.center;
}

-(void)setUpTableView{
    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.sectionIndexColor = [UIColor blueColor];// 改变索引的颜色
    self.tableView.sectionIndexTrackingBackgroundColor = [UIColor grayColor];//改变索引选中的背景颜色
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableView.scrollEnabled = YES;
    

//    索引数组
    _dataSource = [[NSMutableArray alloc] init];
//    初始化数据
    for (char c = 'A'; c <= 'Z'; c++) {
        [_dataSource addObject:[NSString stringWithFormat:@"%c",c]];
    }
    [_dataSource addObject:@"#"];
}
//按拼音分组城市
-(void)seperatorWord{
    _mutableDic = [[NSMutableDictionary alloc] init];
    for (int i = 0; i < [Singleton shareInstance].cityArray.count; i++) {

        CityModel *model =[[Singleton shareInstance].cityArray objectAtIndex:i];

        NSMutableArray *wordArray = [[NSMutableArray alloc] init];
        for (int k = 0; k < _dataSource.count; k++) {
            //字符串变为大写 uppercaseString  小写是lowercaseString
            _str = [[model.py substringToIndex:1] uppercaseString];
            if ([_str isEqualToString:[_dataSource objectAtIndex:k]]) {
                [wordArray addObject:model];
            }
            [_mutableDic setValue:wordArray forKey:_str];
        }
    }
    NSLog(@"mutableDic ---%@",_mutableDic);
}

//多少section
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _mutableDic.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSString *string = [[_mutableDic allKeys] objectAtIndex:section];
    return [[_mutableDic valueForKey:string] count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 30;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellId  = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        NSString *string = [[_mutableDic allKeys] objectAtIndex:indexPath.section];
        CityModel *model = [[_mutableDic valueForKey:string] objectAtIndex:indexPath.row];
        cell.textLabel.text = model.name;
    }
    return cell;
}
#pragma mark - 添加索引
- (nullable NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return _dataSource;
}
//响应点击索引时的委托方法
-(NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index{
    for (int i = 0; i < [_mutableDic count]; i++) {
        NSString *string = [[_mutableDic allKeys] objectAtIndex:i];
        if ([title isEqualToString:string]) {
            NSIndexPath *selectedIndex = [NSIndexPath indexPathForRow:0 inSection:i];
            //返回值不能为空
            index = (NSInteger)selectedIndex;
            [tableView scrollToRowAtIndexPath:selectedIndex atScrollPosition:UITableViewScrollPositionTop animated:YES];
        }
    }
    if (index) {
        self.label.text = title;
        self.label.textAlignment = NSTextAlignmentCenter;
    }
    return index;
}
//section 标题
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSString *string = [[_mutableDic allKeys] objectAtIndex:section];
    return string;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 70, 30)];
    title.text = [_dataSource objectAtIndex:section];
    title.textColor=[UIColor blackColor];
    title.font = [UIFont systemFontOfSize:20];
    title.textAlignment= NSTextAlignmentLeft;
    [tableView.tableHeaderView addSubview:title];
    return tableView.tableHeaderView;
}
@end
