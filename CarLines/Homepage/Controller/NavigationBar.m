//
//  NavigationBar.m
//  CarLines
//
//  Created by 国信 on 16/3/8.
//  Copyright © 2016年 LYY. All rights reserved.
//

#import "NavigationBar.h"
#import "BaseHeader.h"
@implementation NavigationBar

-(void)viewDidLoad{
    [super viewDidLoad];
    
}

-(void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
    [super pushViewController:viewController animated:animated];
    if (self.viewControllers.count > 1) {
        viewController.hidesBottomBarWhenPushed = YES;
        UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [backBtn setImage:[UIImage imageNamed:@"iconfont-iconfontfanhui"] forState:UIControlStateNormal];
        CGSize size = backBtn.currentImage.size;
        backBtn.frame = CGRectMake(0, 0, size.width, size.height);
        viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
        [backBtn addTarget:self action:@selector(backButtonAction) forControlEvents:UIControlEventTouchUpInside];
    }
}

-(void)backButtonAction{
    [self popViewControllerAnimated:YES];
}

@end
