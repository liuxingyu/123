//
//  NavigationBar.h
//  CarLines
//
//  Created by 国信 on 16/3/8.
//  Copyright © 2016年 LYY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavigationBar : UINavigationController

-(void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated;

@end
