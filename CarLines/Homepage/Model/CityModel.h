//
//  CityModel.h
//  CarLines
//
//  Created by 国信 on 16/3/10.
//  Copyright © 2016年 LYY. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CityModel : NSObject
//城市ID
@property (nonatomic, assign) NSNumber *sid;
//城市名称
@property (nonatomic, retain) NSString *name;
//城市拼音
@property (nonatomic, retain) NSString *py;
//首页名
@property (nonatomic, retain) NSString *program_title;
//logo
@property (nonatomic, retain) NSString *index_logo;
//电话
@property (nonatomic, retain) NSString *kf_phone;
//邮箱
@property (nonatomic, retain) NSString *kf_email;
-(instancetype)initWithDictionary:(NSDictionary *)dic;

+(instancetype)modelInitWithDictionary:(NSDictionary *)dic;

@end
