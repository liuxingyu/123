//
//  CityModel.m
//  CarLines
//
//  Created by 国信 on 16/3/10.
//  Copyright © 2016年 LYY. All rights reserved.
//

#import "CityModel.h"

@implementation CityModel

-(void)setValue:(id)value forUndefinedKey:(NSString *)key{
    if ([key isEqual:@"id"]) {
        self.sid = value;
    }
}

-(instancetype)initWithDictionary:(NSDictionary *)dic{
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dic];
    }
    return self;
}

+(instancetype)modelInitWithDictionary:(NSDictionary *)dic{
    return  [[self alloc] initWithDictionary:dic];
}

@end
