//
//  Singleton.m
//  CarLines
//
//  Created by 国信 on 16/3/10.
//  Copyright © 2016年 LYY. All rights reserved.
//

#import "Singleton.h"

@implementation Singleton

static Singleton *_instance = nil;

+(instancetype)shareInstance{
    static dispatch_once_t onceToken ;
    dispatch_once(&onceToken, ^{
        _instance = [[self alloc] init] ;
    }) ;
    
    return _instance ;
}

-(instancetype)init{
    self = [super init];
    if (self) {
        _instance.cityArray = [[NSMutableArray alloc] init];
    }
    return self;
}


@end
