//
//  Singleton.h
//  CarLines
//
//  Created by 国信 on 16/3/10.
//  Copyright © 2016年 LYY. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Singleton : NSObject

+(instancetype)shareInstance;
-(instancetype)init;
//首页标题
@property (nonatomic, retain) NSString *title;
//城市
@property (nonatomic, retain) NSString *cityName;
//电话
@property (nonatomic, retain) NSString *phone;
//邮箱
@property (nonatomic, retain) NSString *email;
//城市数组
@property (nonatomic, retain) NSMutableArray *cityArray;
@end
