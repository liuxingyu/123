//
//  MoreTCell.h
//  CarLines
//
//  Created by 国信 on 16/3/7.
//  Copyright © 2016年 LYY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseHeader.h"
@interface MoreTCell : UITableViewCell
@property (nonatomic, retain) UIImageView *iconImage;
@property (nonatomic, retain) UILabel *titleLabel;
@property (nonatomic,  retain) UIView *lineView;
@end
