//
//  MoreTCell.m
//  CarLines
//
//  Created by 国信 on 16/3/7.
//  Copyright © 2016年 LYY. All rights reserved.
//

#import "MoreTCell.h"

@implementation MoreTCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUpTableViewCell];
    }
    return self;
}

-(void)setUpTableViewCell{
    self.iconImage = [[UIImageView alloc] init];
    [self addSubview:self.iconImage];
    self.iconImage.sd_layout.leftSpaceToView(self, WIDTH(15)).topSpaceToView(self, HEIGHT(15)).heightIs(HEIGHT(40)).widthIs(WIDTH(40));
    
    self.titleLabel = [[UILabel alloc] init];
    [self addSubview:self.titleLabel];
    self.titleLabel.sd_layout.leftSpaceToView(self.iconImage,WIDTH(15)).topSpaceToView(self, HEIGHT(15)).heightIs(HEIGHT(40)).widthRatioToView(self, 0.55);
    
    self.lineView = [[UIView alloc] init];
    [self addSubview:self.lineView];
    self.lineView.sd_layout.leftSpaceToView(self,0).topSpaceToView(self, HEIGHT(69)).heightIs(HEIGHT(1)).widthRatioToView(self,1);
    [self.lineView setBackgroundColor:[UIColor colorWithRed:(239 / 255.) green:(239 / 255.) blue:(239 / 255.) alpha:1]];
    
}

@end
